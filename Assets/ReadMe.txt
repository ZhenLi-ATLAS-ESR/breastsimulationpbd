Title:
Position-based modeling of lesion displacement in Ultrasound-guided breast biopsy

Authors:
Eleonora Tagliabue, Diego Dall�Alba, Enrico Magnabosco, Chiara Tenga, Igor Peterlik, Paolo Fiorini

E-mail:
paolo.fiorini@univr.it